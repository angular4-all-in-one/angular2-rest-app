###node modules installati
```
npm install jquery --save
npm install @types/jquery --save
npm install ngx-bootstrap bootstrap --save
npm install ng2-interceptors --save
npm install angular2-datatable --save
npm install json-server --save
```

###import inseriti in angular.cli.json per far funzionare bootstrap e jquery
```
"scripts": ["../node_modules/jquery/dist/jquery.min.js",
            "../node_modules/bootstrap/dist/js/bootstrap.min.js"],
"styles": ["styles.css",
           "../node_modules/bootstrap/dist/css/bootstrap.min.css"],
```

###codice inserito per far funzionare bootstrap
```
app.module.ts         import { AlertModule } from 'ngx-bootstrap';
app.module.ts         imports: [AlertModule.forRoot(), ... ],
```

###per far partire l'applicazione dovrai eseguire solamente i seguenti comandi
```diff
+ node modules: npm install
+ db locale: npm install -g json-server
+ back-end: json-server --watch db.json
+ webapp: ng serve
```




