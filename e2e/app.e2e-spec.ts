import { Angular2RestAppPage } from './app.po';

describe('angular2-rest-app App', function() {
  let page: Angular2RestAppPage;

  beforeEach(() => {
    page = new Angular2RestAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
