import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
//classes
import { Test } from '../classes/test';
import { AppConstants } from '../classes/app-constants';
//services
import { InterceptorService } from 'ng2-interceptors'; 

@Injectable()
export class TestService {

  private url = AppConstants.ENDPOINT_URI + 'posts/'
  private getAllTestsSource = new Subject<boolean>();
  getAllTests = this.getAllTestsSource.asObservable();

  headers: Headers;
  options: RequestOptions;

  static instance: TestService;
  constructor(private http: InterceptorService) { 
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
    return TestService.instance = TestService.instance || this;
  }

  getLanguageFile(){
    return this.http.get('/assets/language.json')
      .map(this.extractData_lang)
      .catch(this.handleError)
  }

  confirmRequest(ok: boolean) {
    this.getAllTestsSource.next(ok);
  }

  getAllTest_Real(): Observable<Test[]> {
    return this.http.get(this.url)
      .map(this.extractData_get)
      .do(this.elaborateData_get)
      .catch(this.handleError)
  }

  addTest_Real(newTest: Test): Observable<Test> {  
    return this.http.post(this.url, newTest, this.options)
      .map(this.extractData_post)
      .do(this.elaborateData_post)
      .catch(this.handleError);
  }

  deleteTest_Real(idTest: Test): Observable<Test> {
    return this.http.delete(this.url + idTest)
      .map(this.extractData_delete)
      .do(this.elaborateData_delete)
      .catch(this.handleError);
  }

  editTest_Real(test: Test): Observable<Test> {
    return this.http.put(this.url + test.id, test)
      .map(this.extractData_edit)
      .do(this.elaborateData_edit)
      .catch(this.handleError);
  }

  private extractData_lang(res: Response) {
    return res.json();
  }

  private extractData_get(res: Response) {
    return <Test[]>res.json();
  }

  private extractData_post(res: Response) {
    return <Test>res.json();
  }

  private extractData_delete(res: Response) {
    return res.json();
  }

  private extractData_edit(res: Response) {
    return res.json();
  }

  private elaborateData_get(json: Test[])  {
    // console.log("GetALL: " + JSON.stringify(json));
  }

  private elaborateData_post(json: Test)  {
    // console.log("GetNew: " + JSON.stringify(json));
  }

  private elaborateData_delete(json: Test)  {
    // console.log("Get: " + JSON.stringify(json));
  }

  private elaborateData_edit(json: Test)  {
    // console.log("Get: " + JSON.stringify(json));
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
