/* Per utilizzare l'interceptor nei service:
    import { InterceptorService } from 'ng2-interceptors'; 
    constructor(private http: InterceptorService) { }
*/
import { Injectable } from '@angular/core';
import { Interceptor, InterceptedRequest, InterceptedResponse } from 'ng2-interceptors';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
 
@Injectable()
export class InterceptorsService implements Interceptor {

    constructor(private http: Http, private router: Router) { 
        let _build = (<any>http)._backend._browserXHR.build;
        (<any>http)._backend._browserXHR.build = () => {
            let _xhr = _build();
            _xhr.withCredentials = true;
            return _xhr;
        };
    }

    public interceptBefore(request: InterceptedRequest): InterceptedRequest {
        request.options.headers.append('X-Requested-With', 'XMLHttpRequest');
        return request;
    }
 
    public interceptAfter(response: InterceptedResponse): InterceptedResponse {
        console.log(response.response.status);
        return response;
    }
}