import { Injectable } from '@angular/core';
import { Language } from '../classes/language';
import { Dictionary } from '../classes/dictionary';

@Injectable()
export class LanguageService {

    actualLanguage: Language = new Language();
    allLanguages: Dictionary = new Dictionary(this.actualLanguage);

    static instance: LanguageService;
    constructor() {
        return LanguageService.instance = LanguageService.instance || this;
    }

}
