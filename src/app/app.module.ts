import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AlertModule } from 'ngx-bootstrap';
import { Routes, RouterModule } from '@angular/router';
//services
import { InterceptorService } from 'ng2-interceptors';
import { InterceptorsService } from './services/interceptors.service';
import { XHRBackend, RequestOptions } from '@angular/http';
import { TestService } from './services/test.service';
import { LanguageService } from './services/language.service';
//components
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'test', loadChildren: './components/test/test.module#TestModule' },
  { path: '**', redirectTo: '' }
];

export function interceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, interceptorsService:InterceptorsService){
  let service = new InterceptorService(xhrBackend, requestOptions);
  service.addInterceptor(interceptorsService);
  return service;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [LanguageService, TestService, InterceptorsService, 
    {
      provide: InterceptorService,
      useFactory: interceptorFactory,
      deps: [XHRBackend, RequestOptions, InterceptorsService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
