export class Test {
    
    id: number = 0; 
    title: string = '';
    author: string = '';

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}