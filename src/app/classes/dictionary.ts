import { Language } from './language'

export class Dictionary extends Language {
    
    IT: Language = new Language();
    EN: Language = new Language();

    constructor(language: Language) {
        super(language);
    }
}
