import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//classes
import { Test } from '../../../classes/test';
//services
import { TestService } from '../../../services/test.service';
import { LanguageService } from '../../../services/language.service';
//declare jqeury
declare var $:any

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  
  itemAppoggio: Test = new Test();
  errorMessage: string;
  listTest: Test[];

  @Output() allTests: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private testService: TestService, private languageService: LanguageService) { }

  ngOnInit() {
    this.refreshTests();
  }

  refreshTests() {
    this.testService.getAllTest_Real()
      .subscribe(
        tests => this.getAllTest_success(tests),
        error =>  this.errorMessage = <any>error
      );
  }

  getAllTest_success(results: Test[]) {
    this.allTests.emit(true);
    this.listTest = results;
  }

  openModalAddTest() {
    $("#myModalAdd").modal("show"); 
  }

  addTest() {
    var clone = Object.assign({}, this.itemAppoggio);
    this.testService.addTest_Real(clone)
      .subscribe(
        test => this.addTestSuccess(test),
        error =>  this.errorMessage = <any>error
    );
  }

  addTestSuccess(newTest) {
    this.listTest.push(newTest);
    $("#myModalAdd").modal("hide");
    this.itemAppoggio = new Test();
  }

  onDeleteTest(test) {
    this.testService.deleteTest_Real(test.id).subscribe(
      success => success,
      error => console.log('Error: ', error),
      () => this.refreshTests()
    );
  }

  onEditTest(test) {
    this.testService.editTest_Real(test).subscribe(
      success => success,
      error => console.log('Error: ', error),
      () => this.refreshTests()
    );
  }

}
