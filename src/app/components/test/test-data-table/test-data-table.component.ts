import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//classes
import { Test } from '../../../classes/test';
//components
import { TestService } from '../../../services/test.service';
import { LanguageService } from '../../../services/language.service';
//declare jqeury
declare var $:any

@Component({
  selector: 'app-test-data-table',
  templateUrl: './test-data-table.component.html',
  styleUrls: ['./test-data-table.component.css']
})
export class TestDataTableComponent implements OnInit {

  @Input() listTest: Array<Test>;
  @Output() testDelete: EventEmitter<Test> = new EventEmitter<Test>();
  @Output() testEdit: EventEmitter<Test> = new EventEmitter<Test>();

  searchText: string = '';
  errorMessage: string;
  itemAppoggio: Test = new Test();

  constructor(private testService : TestService, private languageService : LanguageService) {  }

  ngOnInit() { }

  openModalDeleteTest(item) {
    var clone = Object.assign({}, item);
    this.itemAppoggio = clone;
    $("#myModalDelete").modal("show"); 
  }

  openModalEditTest(item) {
    var clone = Object.assign({}, item);
    this.itemAppoggio = clone;
    $("#myModalEdit").modal("show"); 
  }

  deleteTest() {
    this.testDelete.emit(this.itemAppoggio);
  }

  editTest() {
    this.testEdit.emit(this.itemAppoggio);
  }

  mySearch() {
    if (this.listTest) {
      if (this.searchText) {
        return this.listTest.filter((item) => {
            return item.title.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1;
        });
      } else {
        return this.listTest;
      }
    }
  }

}
