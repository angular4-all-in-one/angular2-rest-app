import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//classes
import { Test } from '../../../classes/test';
//services
import { TestService } from '../../../services/test.service';

@Component({
  selector: 'app-manage-test',
  templateUrl: './manage-test.component.html',
  styleUrls: ['./manage-test.component.css']
})
export class ManageTestComponent implements OnInit {

  getAllTestsOk: boolean = false;

  constructor(private testService: TestService) { 
      testService.getAllTests.subscribe(
        isAllTests => {
          this.getAllTestsOk = isAllTests;
          console.log("(manage-test-component) getAllTests success =", this.getAllTestsOk);
      });
  }

  ngOnInit() { }

  onGetAllTests(res) {
    this.testService.confirmRequest(res);
  }

}