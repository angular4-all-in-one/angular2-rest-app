import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DataTableModule } from "angular2-datatable";
//services
import { TestService } from '../../services/test.service';
//components
import { TestComponent } from './test/test.component';
import { ManageTestComponent } from './manage-test/manage-test.component';
import { TestDataTableComponent } from './test-data-table/test-data-table.component';

const routes: Routes = [
  { path: '', component: ManageTestComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    DataTableModule
  ],
  providers: [TestService],
  declarations: [TestComponent, ManageTestComponent, TestDataTableComponent]
})
export class TestModule { }
