import { Component } from '@angular/core';
import { Router } from '@angular/router';
//classes
import { Dictionary } from './classes/dictionary'
//services
import { TestService } from './services/test.service';
import { LanguageService } from './services/language.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  getAllTestsOk: boolean = false;

  constructor(private testService: TestService, private languageService: LanguageService,
              private router: Router) { 

    testService.getAllTests.subscribe(
      isAllTests => {
        this.getAllTestsOk = isAllTests;
        console.log("(app-component) getAllTestsOk: ", this.getAllTestsOk);
    });

  }

  ngOnInit() {
    this.getLanguageFile();
  }

  getLanguageFile(){
    this.testService.getLanguageFile().subscribe(
      success => this.extractData(success),
      error => this.getLanguageError()
    );
  }

  changeLanguage() {
    if (localStorage.getItem('lang') == 'IT') {
      this.languageService.actualLanguage = this.languageService.allLanguages.EN;
      localStorage.setItem('lang', 'EN');
    }else{
      this.languageService.actualLanguage = this.languageService.allLanguages.IT;
      localStorage.setItem('lang', 'IT');
    }
  }

  private extractData(dictionary: Dictionary) {
    this.languageService.allLanguages = dictionary;
    if (localStorage.getItem('lang') == null) {
      localStorage.setItem('lang', 'IT');
      this.languageService.actualLanguage = dictionary.IT;
    }else{
      if (localStorage.getItem('lang') == 'IT') {
        localStorage.setItem('lang', 'IT');
        this.languageService.actualLanguage = dictionary.IT;
      }else{
        localStorage.setItem('lang', 'EN');
        this.languageService.actualLanguage = dictionary.EN;
      }
    }
    return this.languageService.actualLanguage;
  }

  private getLanguageError() {
    this.router.navigate(['/']);
  }

}
